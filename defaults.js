const keys = require('./keys')
function defaults(obj, defaults) {
    if ((typeof obj == 'object') && (!(Array.isArray(obj)))) {
        if ((typeof defaults == 'object') && (!(Array.isArray(defaults)))) {
            let keysArray = keys(obj)
            let defaultsKeysArrays = keys(defaults)
            for (let index = 0; index < defaultsKeysArrays.length; index++) {
                if (keysArray.includes(defaultsKeysArrays[index])) { }
                else {
                    obj[defaultsKeysArrays[index]] = defaults[defaultsKeysArrays[index]]
                }
            }
            return obj
        }
    }
}


module.exports = defaults