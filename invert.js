const keys = require('./keys')
function invert(obj) {

    if ((typeof obj == 'object') && (!(Array.isArray(obj)))) {
        let keysArray = keys(obj)
        let requiredJSON = {}
        for (let index = 0; index < keysArray.length; index++) {
            requiredJSON[obj[keysArray[index]]] = keysArray[index]

        }
        return requiredJSON

    }
}

module.exports = invert