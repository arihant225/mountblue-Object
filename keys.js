function keys(obj) {
    if ((typeof obj == 'object') && (!(Array.isArray(obj)))) {
        let requiredObject = JSON.stringify(obj)
        requiredObject = requiredObject.slice(1, -1)
        requiredObject = requiredObject.split(',')
        let keysArray = []
        for (let index = 0; index < requiredObject.length; index++) {
            let temp = (requiredObject[index]).split(':')
            keysArray.push(temp[0].slice(1, -1))

        }
        return keysArray
    }
    else if ((typeof obj == 'object')) {
        let keysArray = []
        for (let index = 0; index < obj.length; index++) {
            keysArray.push(('' + index))

        }
        return keysArray

    }
}
module.exports = keys

