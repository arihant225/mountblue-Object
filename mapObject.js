const keys = require('./keys')
function mapObject(obj, cb) {
    if (((typeof obj == 'object') && (!(Array.isArray(obj)))) && (typeof cb == 'function')) {
        let keysArray = keys(obj)
        let result = {}
        for (let index = 0; index < keysArray.length; index++) {
            let resultantValue = cb(obj[keysArray[index]], keysArray[index], obj)
            result[keysArray[index]] = resultantValue
        }
        return result
    }
}


module.exports = mapObject