const keys = require('./keys')
function pairs(obj) {
    if ((typeof obj == 'object') && (!(Array.isArray(obj)))) {
        let keysArray = keys(obj)
        let pairsArrays = []
        for (let index = 0; index < keysArray.length; index++) {
            pairsArrays.push([keysArray[index], obj[keysArray[index]]])


        }
        return pairsArrays
    }
}
module.exports = pairs