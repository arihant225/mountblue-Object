const keys = require('./keys')
function values(obj) {
    if ((typeof obj == 'object') && (!(Array.isArray(obj)))) {
        let keysArray = keys(obj)
        let valuesArray = []
        for (let index = 0; index < keysArray.length; index++) {
            if (typeof obj[keysArray[index]] != 'function') {
                valuesArray.push(obj[keysArray[index]])
            }
        }

        return valuesArray

    }
}

module.exports = values